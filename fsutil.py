#!./env/bin/python3
import re
import os
from pathlib import Path

from nodes.nodes import NodeManager


class FileSystem:
    '''The main class; used for directional operations.'''
    def __init__(self, append=""):
        self.nodes = NodeManager()
        self.append = append
        self.append_maps = {
            "cwd":os.getcwd(),
            "home":Path.home(),
        }
        self.shortcut_maps = {
            "??":"",
            "?/":os.getcwd(),
        }

    def _replace(self, _type, _working_node):
        return _working_node.replace(_type, "")

    def _check_append_value(self):
        if self.append in self.append_maps:
            self.append = self.append_maps[self.append]

    def _parse_shortcut(self, _working_node, _type, _value):
        _working_node = self._replace(_type, _working_node)
        self.append = _value
        return _working_node

    def _check_shortcuts(self, _working_node):
        for _type, _value in self.shortcut_maps.items():
            if _type in _working_node:
                return self._parse_shortcut(_working_node, _type, _value)
        return None

    def path(self, working_node=""):
        '''The communicator function to pass values to the NodeManager
        Note: working_node can be either a directory or file.'''
        self._check_append_value()

        _node = self._check_shortcuts(working_node)
        if _node:
            working_node = _node

        self.nodes(working_node, self.append)
        return self.nodes


if __name__ == '__main__':
    fs = FileSystem()
    fs.path("?/example.md")
    print(fs.path().read())
