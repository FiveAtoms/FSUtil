import pytest
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from fsutil import FileSystem, Parser
from nodes.nodes import Directory, File
import filetypes.filetypes


'''
DUMMY DATA
'''
parser_is_directory = ["this/is/directory", "/this/is/directory/", "this/is/directory/", "/this/is/directory"]
parser_is_file = ["/path/to/file.py/", "path/to/file.py", "/path/to/file.py", "path/to/file.py/"]

parser_values = ["this/is/directory", "/this/is/directory", "/this/is/directory/", "this/is/directory/"]
parser_correct_value = "/this/is/directory/"


class BaseTest:
    def setup_class(cls):
        cls.fs = FileSystem()
        cls.pr = Parser()

    def teardown_class(cls):
        del cls.fs
        del cls.pr


# fsutil.py
class TestParser(BaseTest):
    def test_is_directory(self):
        for directory in parser_is_directory:
            assert self.pr.is_directory(directory) == True
    def test_is_file(self):
        for file in parser_is_file:
            assert self.pr.is_file(file) == True
    def test_parse_value(self):
        for value in parser_values:
            assert self.pr.parse_value(value) == parser_correct_value


# fsutil.py
class TestFileSystem:
    pass


# nodes/nodes.py
class TestNodeDirectory:
    pass

# nodes/nodes.py
class TestNodeFile:
    pass