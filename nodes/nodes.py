import shutil
import re
import os

from nodes.parser import Parser
from nodes.nodetypes import File, Directory


class InvalidOperationOnGivenValue(Exception):
    pass


class NodeManager:
    '''Parsing and directional operations; shared methods between nodes.'''
    def __init__(self):
        self.working_node = None
        self.is_file = None
        self.file = File()
        self.directory = Directory()
        self.parser = Parser()

    def check_if(self, value, expected):
        '''I called this function "check_if" so that when you read the line,
        it will go as follows: "check if the working node is a file." Now that's
        read()-ability!'''
        type_of = "is_file" if self.is_file else "is_directory"
        if type_of != expected:
            raise InvalidOperationOnGivenValue("You attempted to perform a {} operation on a value that is a {}.".format(expected.replace("is_", ""), type_of.replace("is_", "")))

    def read(self):
        self.check_if(self.working_node, "is_file")
        return self.file.read(self.working_node)

    def write(self, content, open_type="w"):
        self.check_if(self.working_node, "is_file")
        self.file.write(self.working_node, content, open_type)

    def copy(self, location):
        if self.is_file:
            self.file.copy(self.working_node, location)
        else:
            self.directory.copy(self.working_node, location)

    def archive(self, archive_type="zip"):
        if self.is_file:
            self.working_node = self.file._parse_archive_value(self.working_node)
        self.directory.archive(self.working_node, archive_type)

    # Shared methods
    def move(self, location):
        shutil.move(self.working_node, location)

    def create(self):
        if self.is_file:
            self.file.create(self.working_node)
        else:
            self.directory.create(self.working_node)


    def _parse_append(self, value):
        if value != "":
            if value[-1] != "/":
                value = ''.join([value, "/"])
        return value

    def _parse_working_node(self, value):
        if value != "":
            if value[0] == "/":
                split_node = re.split("^/", value)
                value = split_node[1]
        return value

    def _parse_nodes(self, append_value, working_node):
        parsed_nodes = (self._parse_append(append_value),
                        self._parse_working_node(working_node))
        return ''.join(parsed_nodes)

    def __call__(self, working_node, append):
        if working_node != "":
            if append != "":
                parsed_working_node = self._parse_nodes(append, working_node)
            else:
                parsed_working_node = self.parser.parse(working_node)

            self.working_node = parsed_working_node
            self.is_file = self.parser.is_file(parsed_working_node)
