import re
import json
import os
import shutil

from nodes.regex import Regex

gex = Regex()

class Directory:
    def copy(self, src, to):
        shutil.copytree(src, to)

    def create(self, directory):
        os.mkdir(directory)

    def archive(self, directory, archive_type):
        shutil.make_archive(directory, archive_type, directory)


class File:
    '''
    @about:
        The File class; file operations on multiple file_types.
        Note: kept the logic in a single class because two extra
        file_type classes would've added complexity and reduced
        readability.
    '''
    def _parse_archive_value(self, working_node):
        before_extension = gex.get("b4ext", working_node)

        try:
            os.mkdir(before_extension)
        except FileExistsError: pass
        finally:
            self.copy(working_node, before_extension)

        return ''.join([before_extension, "/"])


    def get_type_of(self, value):
        '''Get extension of file path without period'''
        file_split = os.path.splitext(value)
        return file_split[1].replace(".", "")

    def write(self, file_name, content, open_type="w"):
        if self.get_type_of(file_name) == "json":
            content = json.dumps(content)

        with open(file_name, open_type) as open_file:
            open_file.write(content)

    def read(self, file_name):
        with open(file_name, "r") as open_file:
            if self.get_type_of(file_name) == "json":
                return json.load(open_file)
            else:
                return open_file.read()

    def copy(self, src, to):
        shutil.copy(src, to)

    def create(self, file_name):
        self.write(file_name, "")

    def move(self, file_name, location):
        shutil.move(file_name, location)