import re


class Regex:
    def __init__(self):
        self._maps = {
            "ext": self._get_ext,
            "b4ext": self._get_b4ext,
            "fname": self._get_fname,
        }

    def _get_ext(self, value):
        split_value = re.split("\.", value)
        return ''.join([".", split_value[1]])

    def _get_b4ext(self, value):
        split_value = re.split("\.", value)
        return split_value[0]

    def _get_fname(self, value):
        split_value = re.split("\/.*$", value)
        return split_value[1]

    def get(self, get_type, value):
        return self._maps[get_type](value)