import re


class Parser:
    '''A more proper way to do the following 2 methods would be
    os.path.isfile and os.path.isdir methods, respectively. I kept it this
    way to allow you to use values of files or directories that don't yet exist.'''
    def is_directory(self, value):
        return True if "." not in value else False
    def is_file(self, value):
        return True if "." in value else False

    def parse_value(self, value):
        '''Parse any value put into FileSystem. It really only
        deals with slashes to make your life easier.'''

        mod_val = ""

        if value == "" or value == None:
            return value

        mod_val = value

        if value[0] != "/":
            mod_val = "/" + value

        if value[-1] != "/":
            if self.is_directory(value):
                mod_val = mod_val + "/"
        else:
            if self.is_file(value):
                split_val = re.split("/$", value)
                mod_val = split_val[0]
        return mod_val

    def parse(self, value):
        return self.parse_value(value)