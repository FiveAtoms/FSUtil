# FSUtil

File System Utility (FSUtil), is a useful file system module for no-brainer file and directory operations. It is used to make file and directory handling easier, faster, and cleaner.

### Note

This is only going to be passively maintained.

## Features

- Set the working file or directory. For ease of use, you can set a working file once and then simply run functions on the working file. View an example below.
- Set an append value. To make your life easier, you can, upon instantization, set an append value, that is, a path which you will be working in, and then you can simply name and use files from that append value. This value is appended to the beginning of every path() value you pass in. So, for example, you can set your append value to "/home/user/" and then you can access files simply by using "fs.path("example.txt")," this will come out to "/home/user/example.txt." See another example below.
-

### Feature Examples

Working File or Directory:

    fs = FileSystem()

    # Set the file
    fs.path("example.txt")

    # This will use "example.txt"
    print(fs.path().read())

Append value:

    fs = FileSystem(append="/home/user/apps/myapp/")
    fs.path("example.txt")
    print(fs.path().read())

    # The above will print the contents of "/home/user/apps/myapp/example.txt"

    fs.path("another-example.txt")
    print(fs.path().read())

    # The above will print the contents of "/home/user/apps/myapp/another-example.txt"


## Usage information

Note: on all of the below examples, you can assume FileSystem was instantiated with the following line:

Basic usage:

    fs = FileSystem()

Setting an append value:

    fs = FileSystem("/home/user/data/")

    # Now "/home/user/data/" will be appended to the beginning of the following two lines.
    fs.path("example.txt").read()
    fs.path("path/to/new/folder/").create()

Setting a working file:

    fs.path("working-file.txt")

Setting a working directory:

    fs.path("/working/directory/")

Read from a file:

    fs.path("example.txt").read()

Write to a file:

    # open_type defaults to "w"
    fs.path("example.txt").write("content", open_type="w")

Copy a file or directory to a new location:

    fs.path("example.txt").copy("path/to/new/location")
    fs.path("/path/to/folder").copy("/new/location/")

Move a file or folder:

    fs.path("example.txt").move("/new/location/")

Create a file or folder:

    fs.path("new-file.txt").create()


## Useful tips

- If you set an append value upon initialization, you can still use absolute paths without the append value if you add "??" to the beginning of the path. You can then work freely with that absolute path. Usage example: "fs.path("??/home/user/docs/example.txt").read()"
- To append the current working directory regardless of whether the append value is set, start your path off with "?/" This will cause the current working directory to be appended to the front. The main append value will not be used if "?/" is found.